package uk.co.borismoris.slack.weblient;

import be.olsson.slackappender.client.Client;

import java.io.IOException;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import static java.net.http.HttpClient.Redirect.NORMAL;
import static java.net.http.HttpClient.Version.HTTP_1_1;

public class HttpclientSlackClient implements Client {

    private final HttpClient httpClient;

    public HttpclientSlackClient() {
        httpClient = HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .version(HTTP_1_1)
                .cookieHandler(new CookieManager())
                .build();
    }

    @Override
    public void send(final URL webhookUrl, final String payload) {
        if (webhookUrl == null) {
            return;
        }
        final HttpRequest request;
        try {
            request = HttpRequest.newBuilder()
                    .uri(webhookUrl.toURI())
                    .POST(BodyPublishers.ofString(payload))
                    .header("Accept", "*/*")
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to convert Slack URL to URI", e);
        }
        final HttpResponse<String> response;
        try {
            response = httpClient.send(request, BodyHandlers.ofString());
        } catch (IOException e) {
            throw new RuntimeException("Failed to send http request", e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (response.statusCode() != HttpURLConnection.HTTP_OK) {
            System.err.printf("Failed to send Slack message %s %s%n", response.statusCode(), response.body());
        }
    }
}
