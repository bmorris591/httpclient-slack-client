package uk.co.borismoris.slack.weblient;

import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HttpclientSlackClientTest {

    @RegisterExtension
    public static final WireMockExtension wiremock = WireMockExtension.newInstance()
            .options(options()
                    .dynamicPort()
                    .usingFilesUnderClasspath("uk/co/borismoris/slack/weblient/wiremock")
                    .notifier(new Slf4jNotifier(true)))
            .configureStaticDsl(true)
            .build();

    HttpclientSlackClient client;

    @BeforeEach
    void setup() {
        client = new HttpclientSlackClient();
    }

    @Test
    void whenAMessageIsSent_thenMessageIsSent(final WireMockRuntimeInfo runtimeInfo) throws MalformedURLException {
        var webhookUrl = new URL(runtimeInfo.getHttpBaseUrl() + "/slack/webhook?auth=someauthvariable");
        var payload = "Payload Payload Payload";
        assertThatCode(() -> client.send(webhookUrl, payload)).doesNotThrowAnyException();

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/slack/webhook")));
    }

    @Test
    void whenResponseIsFault_thenErrorIsPropagated(final WireMockRuntimeInfo runtimeInfo) throws MalformedURLException {
        var webhookUrl = new URL(runtimeInfo.getHttpBaseUrl() + "/slack/webhook?auth=somefaultvariable");
        var payload = "Payload Payload Payload";
        assertThatThrownBy(() -> client.send(webhookUrl, payload))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Failed to send http request")
                .hasRootCauseInstanceOf(IOException.class);

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/slack/webhook")));
    }

    @Test
    void whenResponseIsError_thenNoExceptionIsThrown(final WireMockRuntimeInfo runtimeInfo) throws MalformedURLException {
        var webhookUrl = new URL(runtimeInfo.getHttpBaseUrl() + "/slack/webhook?auth=someerrorauthvariable");
        var payload = "Payload Payload Payload";
        assertThatCode(() -> client.send(webhookUrl, payload)).doesNotThrowAnyException();

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/slack/webhook")));
    }

    @Test
    void whenWebhookURLIsMull_thenNoErrorIsThrown() {
        assertThatCode(() -> client.send(null, null)).doesNotThrowAnyException();
    }
}
