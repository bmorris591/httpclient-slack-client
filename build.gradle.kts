import net.researchgate.release.GitAdapter
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    `java-library`
    jacoco
    `maven-publish`
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.38.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11

    withJavadocJar()
    withSourcesJar()
}

group = "uk.co.borismorris.slack.httpclient"

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.apache.logging.log4j:log4j-bom:2.14.1"))

    compileOnly("be.olsson:slack-appender:1.3.0")

    testImplementation(platform("org.junit:junit-bom:5.8.1"))

    testImplementation("be.olsson:slack-appender:1.3.0")
    testImplementation(platform("org.eclipse.jetty:jetty-bom:9.4.43.v20210629"))
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.31.0") {
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-client")
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-server")
        exclude(group = "org.conscrypt")
        exclude(group = "commons-fileupload")
    }
    testImplementation("org.eclipse.jetty:jetty-alpn-java-server")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core:3.21.0")
    testImplementation("org.awaitility:awaitility:4.1.0")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testRuntimeOnly("org.apache.logging.log4j:log4j-core")
    testRuntimeOnly("org.apache.logging.log4j:log4j-slf4j-impl")
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        exceptionFormat = TestExceptionFormat.FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}

jacoco {
    toolVersion = "0.8.7"
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.isEnabled = true
    }
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = "0.8".toBigDecimal()
            }
        }
    }
}

release {
    pushReleaseVersionBranch = "master"
    preCommitText = "[release]"
    val git = propertyMissing("git") as GitAdapter.GitConfig
    git.requireBranch = ""
}

publishing {
    publications {
        create<MavenPublication>("httpclient-slack-client") {
            from(components["java"])

            pom {
                name.set(project.name)
                description.set("Java 11 HttpClient implementation of the HTTP REST client for the Slack Log4j2 appender")
                url.set("https://gitlab.com/bmorris591/httpclient-slack-client/-/wikis/home")

                scm {
                    url.set("https://gitlab.com/bmorris591/httpclient-slack-client")
                    connection.set("git@gitlab.com:bmorris591/httpclient-slack-client.git")
                    developerConnection.set("git@gitlab.com:bmorris591/httpclient-slack-client.git")
                }

                licenses {
                    license {
                        name.set("GNU GPLv3")
                        url.set("https://gitlab.com/bmorris591/httpclient-slack-client/blob/master/LICENSE")
                        distribution.set("repo")
                    }
                }

                developers {
                    developer {
                        id.set("bmorris591")
                        name.set("Boris Morris")
                        email.set("bmorris591@gmail.com")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            name = "gitlab-ci"
            url = uri("https://gitlab.com/api/v4/projects/26731120/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("token-header")
            }
        }
    }
}
